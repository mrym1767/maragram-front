import React  from 'react';
import {BrowserRouter} from "react-router-dom";
import ImageGallery from "./Pages/ImageGallery";
import GlobalState from "./Context/GlobalState";

function App() {
  return (
      <BrowserRouter>
          <GlobalState>
              <ImageGallery/>
          </GlobalState>
      </BrowserRouter>
        )
}

export default App;
