import React, {useState} from "react";
import ShowImage from "../../Utils/ImageLoader";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {addPhoto} from "../../Services/Servise";

const AddImage = ({match}) => {
    const images=useSelector(state=>state.myImages);
    const [chooseImage,setChooseImage]=useState();

    const handleAdd = event => {
        event.preventDefault();
        try{
            const{data}=addPhoto(match.params.id,chooseImage);
            console.log(data);
        }catch (e) {
            console.log(e)
        }
    }

    return(
        <div className="modal fade" id="addPhoto" tabIndex="-1" role="dialog" aria-labelledby="addPhoto" aria-hidden="true" style={{background: "hsla(0, 100%, 100%, 0.9)"}}>
            <div className="modal-dialog" role="document">
                <div className="modal-content" style={{boxShadow: "0px 10px 50px hsla(0, 0%, 0%, 0.33)" }}>
                    <div className="modal-header bg-white">
                        <h5 className="modal-title" id="exampleModalLabel">انتخاب عکس</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body" style={{backgroundColor:"#f7f7f7"}}>
                        <form>
                        {images.map(image=>(
                            <div className="image-container" key={image.id}>
                                <div id="add-image" style={image.id===chooseImage?{border: "2px solid #aaa9a9"}:{}}>
                                    <NavLink to="#" onClick={()=>setChooseImage(image.id)}>
                                        <ShowImage image={image.url} ></ShowImage>
                                    </NavLink>
                                </div>
                            </div>
                        ))}
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-block btn-outline-dark" onClick={handleAdd}>
                            افزودن
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )}

    export default AddImage;