import React, {useState} from "react";
import {errorMessage, successMessage} from "../../Utils/Messages";
import {newGallery} from "../../redux/action/myGalleries";

const CreateGallery = () => {
    const [title,setTitle]=useState();
    const handleChange= event => {
        setTitle(event.target.value)
    }
    const handleCreate= async event => {
        event.preventDefault();
        try{
            const {data,status} = await newGallery(title);
            if(status===200){
                successMessage("گالری جدید اضافه شد.")
            }
        }catch (e) {
            errorMessage(e)
        }
    }
    return(
            <div className="modal fade" id="createGallery" tabIndex="-1" role="dialog" aria-labelledby="createGallery" aria-hidden="true" style={{background: "hsla(0, 100%, 100%, 0.9)"}}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{boxShadow: "0px 10px 50px hsla(0, 0%, 0%, 0.33)" }}>
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">گالری جدید</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div className="modal-body" style={{backgroundColor:"#f7f7f7"}}>
                            <form>
                                <div className="form-group">
                                    <input type="text" className="form-control" placeholder="نام گالری" onChange={handleChange} />
                                </div>
                            </form>
                        </div>

                        <div className="modal-footer">
                            <button onClick={handleCreate} className="btn btn-block btn-outline-dark" >افزودن</button>
                        </div>
                        </div>
                    </div>
                </div>
    )
}

export default CreateGallery;