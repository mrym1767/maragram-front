import React, {useState} from "react";
import "@reach/dialog/styles.css";
import {useDispatch} from "react-redux";
import {errorMessage, successMessage} from "../../Utils/Messages";
import {newImage} from "../../redux/action/myImages";

const UploadImage = ({showDialog,closeDialog}) => {
    const [selectedFile,setSelectedFile]=useState();
    const [tag,setTag]=useState();
    const [tags,setTags]=useState([]);
    const [description,setDescription]=useState();
    const dispatch = useDispatch();

    const handleSetTag = event => {
        // event.preventDefault();
        // const copyTags=[...tags];
        // console.log(description)
        // console.log(tag);
        // copyTags.push(tag);
        // setTags(copyTags);
        // console.log(tags);
    }

    const handleChange = event => {
        setSelectedFile(event.target.files[0]);
    };

    const handleUpload = async event => {
        event.preventDefault();
        const data = new FormData();
        data.append('photo', selectedFile,selectedFile.name);
        data.append('description',description);
        // data.append('tags',tags);

        try{
            dispatch(newImage(data));
            successMessage("عکس جدید اضافه شد.")
        }catch (e) {
            errorMessage("مشکلی پیش آمده");
            console.log(e);
        }
    };

    return(
        <div className="modal fade" id="uploadImage" tabIndex="-1" role="dialog" aria-labelledby="uploadImage"
             aria-hidden="true" style={{background: "hsla(0, 100%, 100%, 0.9)"}}>
            <div className="modal-dialog" role="document">
                <div className="modal-content" style={{boxShadow: "0px 10px 50px hsla(0, 0%, 0%, 0.33)" }}>
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">اپلود عکس</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body" style={{backgroundColor:"#f7f7f7"}}>
                        <form>
                            <div className="form-group">
                                <input type="file" className="form-control" onChange={handleChange} />
                            </div>

                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="توضیحات" onChange={e=>setDescription(e)}/>
                            </div>

                            <div className="form-group">
                                <input type="text" className="form-control" placeholder="تگ" onChange={e=>setTag(e)}/>
                                {/*<div className="input-group-append">*/}
                                {/*    <button onClick={handleSetTag}><i className="fa fa-plus"></i></button>*/}
                                {/*</div>*/}
                            </div>
                            <div>{tags}</div>
                        </form>
                    </div>
                    <div className="modal-footer">
                        <button className="btn btn-block btn-outline-dark" onClick={handleUpload}>اپلود</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UploadImage;