import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {NavLink, Redirect} from "react-router-dom";


const AllGalleries = () => {
    const dispatch=useDispatch();
    const galleries=useSelector(state=>state.allGalleries);
    const user=useSelector(state=>state.user);

    if(!user.id){
        return <Redirect to="/Login" />
    }else {

        return (
            <div className="margin-top-sm">
                {galleries.length === 0 ? (
                    <div className="no-image">گالری برای نمایش وجود ندارد.</div>
                ) : (
                    galleries.map(gallery => (
                        <div key={gallery.id} className="all-galleries mx-auto rel-position">
                            <NavLink to={`/galleries/${gallery.id}`} className="gallery-name stretched-link">
                                <h5>
                                    <i className="fa fa-photo"></i>
                                    {" گالری " + gallery.title}
                                </h5>
                            </NavLink>

                            <h6>
                                <i className="fa fa-user"></i>
                                {" " + gallery.owner.firstName + " " + gallery.owner.lastName}
                            </h6>
                        </div>
                    ))
                )}

            </div>
        )
    }
}

export default AllGalleries;