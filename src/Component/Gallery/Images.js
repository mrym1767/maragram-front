import React, {useEffect} from "react";
import {useSelector,useDispatch} from "react-redux";
import ShowImage from "../../Utils/ImageLoader";
import {galleryPosts} from "../../redux/action/galleryPosts";
import {disLikePhoto, likePhoto} from "../../Services/Servise";
import {disLikePhotos, likePhotos} from "../../redux/action/like";

const Images = ({match}) => {
    const dispatch=useDispatch();
    const gallery=useSelector(state=>state.galleryPosts);
    const liked=useSelector(state=>state.likePost);

    useEffect(()=>{
        dispatch(galleryPosts(match.params.id));
        console.log(gallery);
    },[])


    return(
        <div className="margin-top-sm">
            {gallery.length===0 ? (
                <div className="no-image">
                    <p>در این گالری عکسی وجود ندارد.</p>
                </div>
            ) : (
                gallery.map(gallery=>(
                    <div key={gallery.id} className="all-gallery mx-auto" >
                        <div className="text-right p-2">
                            {gallery.photo.owner.firstName+" "+gallery.photo.owner.lastName+" "}
                            <i className="fa fa-user"></i>
                        </div>

                        <ShowImage image={gallery.photo.url} />

                        <div className="text-right p-2">
                            {gallery.likes}
                                <i id="heart-like" className={gallery.liked? "fa fa-2x fa-heart" : "fa fa-2x fa-heart-o"}
                                   onClick={()=>{gallery.liked?
                                       dispatch(disLikePhotos(match.params.id,gallery.id))
                                       :
                                       dispatch(likePhotos(match.params.id,gallery.id))}}>
                                </i>
                        </div>

                        <div>
                            {gallery.description}
                        </div>
                    </div>
                ))
            )}
        </div>
    )
}

export default Images;