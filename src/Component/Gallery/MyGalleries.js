import React from "react";
import {NavLink} from "react-router-dom";
import {useSelector} from "react-redux";

const MyGalleries = () => {
    const myGalleries=useSelector(state=>state.myGalleries);

    return(
            <div className="margin-top-sm">
                <div className="add">
                    <button type="button" className="btn"  data-toggle="modal" data-target="#createGallery">
                        <i className="fa fa-2x fa-plus" title="افزودن گالری"></i>
                    </button>
                </div>
                {myGalleries.length===0? (
                        <div className="no-image">گالری برای نمایش وجود ندارد.</div>
                    ) : (
                        myGalleries.map(gallery=>(
                            <div key={gallery.id} className="galleries-container">
                                <div className="galleries-section text-center">
                                    <NavLink to={`/MyPage/MyGalleries/${gallery.id}`} className="btn">
                                        <h4>گالری {gallery.title}</h4>
                                    </NavLink>
                                    {/*<p>{gallery.createdAt}</p>*/}
                                </div>
                            </div>
                        )))}
            </div>
    )
}

export default MyGalleries;