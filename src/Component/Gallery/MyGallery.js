import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {galleryPosts} from "../../redux/action/galleryPosts";
import ShowImage from "../../Utils/ImageLoader";

const MyGallery = ({match}) => {
    const dispatch=useDispatch();
    const gallery=useSelector(state=>state.galleryPosts);

    useEffect(()=>{
        dispatch(galleryPosts(match.params.id));
    },[])

    return (
            <div className="margin-top-sm">
                <div className="add">
                    <button type="button" className="btn"  data-toggle="modal" data-target="#addPhoto">
                        <i className="fa fa-2x fa-plus" title="افزودن عکس"></i>
                    </button>
                </div>

                {!gallery ? (
                    <div className="no-image">
                        <p>در این گالری عکسی وجود ندارد.</p>
                    </div>
                ) : (
                    gallery.map(gallery=>(
                        <div key={gallery.id} className="image-container" >
                            <div className="image-section" id="my-gallery">
                                <ShowImage image={gallery.photo.url} />
                                <div className="image-overlay">
                                    <div className="text-heart">
                                        <i className="fa fa-2x fa-heart"></i>
                                        <h3>{gallery.likes}</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))
                )}
            </div>
    )
}

export default MyGallery;