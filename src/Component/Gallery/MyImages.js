import React, {useContext, useState} from "react";
import MyPage from "../../Pages/MyPage";
import Context from "../../Context/Context";
import {useSelector} from "react-redux";
import ShowImage from "../../Utils/ImageLoader";
import {NavLink} from "react-router-dom";

const MyImages = () => {
    const myImages=useSelector(state=>state.myImages);

    return(
            <div className="margin-top-sm">
                <div className="add">
                    <button type="button" className="btn" data-toggle="modal" data-target="#uploadImage">
                        <i className="fa fa-2x fa-plus" title="اپلود عکس"></i>
                    </button>
                </div>
                {myImages.length===0? (
                        <div className="no-image">عکسی برای نمایش وجود ندارد.</div>
                    ) : (
                        myImages.map(image=>(
                            <div className="image-container" key={image.id}>
                                <div className="image-section" id="my-images">
                                    <NavLink to="#">
                                        <ShowImage image={image.url} ></ShowImage>
                                    </NavLink>
                                    <div className="image-overlay"></div>
                                </div>
                            </div>
                        )))
                }
            </div>

    )
}

export default MyImages;