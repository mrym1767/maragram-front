import React from "react";
import { NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";

const Navbar = () => {
    return(
        <nav className="navbar navbar-expand-sm navbar-light rtl mainNav">
            <NavLink className="navbar-brand" to="/" > <img src="/../img/logo1.png" /> </NavLink>
            <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">

                    <li className="nav-item">
                        <NavLink className="nav-link" to="/MyPage/MyImages">
                            <i className="fa fa-lg fa-user-circle"></i>
                        </NavLink>
                    </li>

                    <li className="nav-item">
                        <NavLink className="nav-link" exact to="/">
                            <i className="fa fa-lg fa-home"></i>
                        </NavLink>
                    </li>

                    <li className="nav-item">
                        <NavLink className="nav-link" to="/Logout">
                            <i className="fa fa-lg fa-sign-out"></i>
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default Navbar;