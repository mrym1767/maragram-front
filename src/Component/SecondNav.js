import React, {useContext} from "react";
import {NavLink} from "react-router-dom";
import Context from "../Context/Context";

const SecondNav = () => {
    const context=useContext(Context);
    return(
        <div className="container mt-4 pt-4 mx-auto">

            {/*<ul className="nav nav-tabs justify-content-center">*/}
            {/*    <li className="nav-item">*/}
            {/*        <NavLink className="nav-link" to="/MyPage" style={{color:"gray"}} activeStyle={{borderTop:"1.40px solid black",backgroundColor:"#f7f7f7"}}>عکس های من</NavLink>*/}
            {/*    </li>*/}


            {/*    <li className="nav-item pl-5">*/}
            {/*            <NavLink className="nav-link" to="/MyPage/MyGalleries" style={{color:"gray"}}>گالری های من</NavLink>*/}
            {/*    </li>*/}
            {/*</ul>*/}

            <nav className="navbar navbar-expand navbar-light rtl" style={{color:"black",borderTop: "1px solid #dbdbdb",padding:"5px 20px"}}>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mx-auto">

                        <li className="nav-item pr-4" style={{color:"gray",paddingTop:" 0",marginTop:"0"}}>
                            <NavLink className="nav-link" to="/MyPage/MyImages" style={{color:"gray",paddingTop:" 0",marginTop:"0"}} activeStyle={{borderTop:"1.33px solid gray"}}>عکس های من</NavLink>
                        </li>


                        <li className="nav-item">
                            <NavLink className="nav-link" to="/MyPage/MyGalleries" style={{color:"gray",paddingTop:" 0",marginTop:"0"}} activeStyle={{borderTop:"1.33px solid gray"}}>گالری های من</NavLink>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default SecondNav;