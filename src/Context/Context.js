import React , {createContext}from "react";

const Context= createContext(
    {
        firstName:"",
        setFirstName:()=>{},
        lastName:"",
        setLastName:()=>{},
        email:"",
        setEmail:()=>{},
        password:"",
        setPassword:()=>{},
        valid:null,
        setValid:0,
        rules:false,
        setRules:false,
        loader:false,
        setLoader:()=>{},

        openDialog:()=>{},
        handleSubmitRegister:()=>{},
        handleSubmitLogin:()=>{},
    }
)

export default Context;