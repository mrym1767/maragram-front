import React, {useRef, useState} from "react";
import Context from "./Context";
import {withRouter} from "react-router-dom";
import jwt from "jsonwebtoken";
import {loginUser, registerUser} from "../Services/Servise";
import SimpleReactValidator from "simple-react-validator";
import {useDispatch} from "react-redux";
import {setUser} from "../redux/action/user";
import {myImages} from "../redux/action/myImages";
import {myGalleries} from "../redux/action/myGalleries";
import {hideLoading, showLoading} from "react-redux-loading-bar";
import {allGallery} from "../redux/action/allGallery";
import {errorMessage, successMessage} from "../Utils/Messages";
import UploadImage from "../Component/Dialog/UploadImage";

const GlobalState = (props) => {
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const[rules,setRules]=useState();
    const [, setValid] = useState();
    const [loader,setLoader]=useState(false);
    const [showDialog,setShowDialog]=useState(false);

    const dispatch=useDispatch();

    const valid = useRef(new SimpleReactValidator({
        messages: {
            required: "پر کردن این فیلد الزامی است",
            alpha: "فقط از حروف استفاده کنید",
            min: "تعداد حروف نباید کمتر از 5 کارکتر باشد",
            max: "تعداد حروف نباید بیشتر از 15 کارکتر باشد",
            email: "ایمیل وارد شده صحیح نیست",
            accepted:"پذیرفتن این فیلد الزامی است"
        }, element: message => <div style={{color: "red"}}>{message}</div>
    }));

    const openDialog= () => setShowDialog(true);
    const closeDialog= () => setShowDialog(false);

    const handleSubmitRegister = async event => {
        event.preventDefault();
        const user = {
            firstName,
            lastName,
            gender: "FEMALE",
            email,
            password,
        };
        console.log(user);
        try { if (valid.current.allValid()){
            const {status} = await registerUser(user);
            dispatch(showLoading());
            if (status === 200) {
                successMessage("ثبت نام شما با موفقیت انجام شد.");
                dispatch(hideLoading());
                props.history.push("/Login");
            }
        }else {
            valid.current.showMessages();
            setValid(1);
        }} catch (e) {
            console.log("catch: " + e);
            dispatch(hideLoading());
        }
    }


    const handleSubmitLogin = async event => {
        event.preventDefault();
        const user = {
            email,
            password }
        try {
            if (valid.current.allValid()) {
                const {status, data} = await loginUser(user);
                if (status === 200) {
                    console.log(data.token)
                    await localStorage.setItem("token", data.token);
                    // const decodeToken=jwt.decode(data.token,{complete:true})
                    await dispatch(setUser());
                    await dispatch(myImages());
                    await dispatch(myGalleries());
                    await dispatch(allGallery());
                    successMessage("با موفقیت وارد شدید.");
                    setLoader(false);
                    props.history.replace("/")
                }
            } else {
                valid.current.showMessages();
                setValid(2)
            }
        } catch (e) {
            errorMessage("مشکلی پیش آمده");
            console.log(e);
        }
    }

    return (
        <Context.Provider value={{
            firstName,
            setFirstName,
            lastName,
            setLastName,
            email,
            setEmail,
            password,
            setPassword,

            rules,
            setRules,
            valid:valid,
            setValid,
            loader,
            setLoader,

            openDialog:openDialog,
            handleSubmitRegister:handleSubmitRegister,
            handleSubmitLogin:handleSubmitLogin
        }}>
            {props.children}
            <UploadImage showDialog={showDialog} closeDialog={closeDialog}/>
        </Context.Provider>
    )
}
export default withRouter(GlobalState);