import React, {Fragment} from "react";
import {Route, withRouter} from 'react-router-dom';
import CreateGallery from "../Component/Dialog/CreateGallery";
import AddImage from "../Component/Dialog/AddImage";
import Navbar from "../Component/Navbar";
import {Footer} from "../Component/Footer";
import UploadImage from "../Component/Dialog/UploadImage";
import {ToastContainer} from "react-toastify";


const MainLayout = (props) => {
    return(
        <Fragment>
            <div className="main-container rtl">
                {props.location.pathname==="/Login" || props.location.pathname==="/Register" ? null : <Navbar/>}
                {props.children}
                <Route path="/MyPage/MyGalleries/:id" component={AddImage} />
                <UploadImage/>
                <CreateGallery/>
            </div>
            <Footer/>
            <ToastContainer/>
        </Fragment>
    )
}

export default withRouter(MainLayout);