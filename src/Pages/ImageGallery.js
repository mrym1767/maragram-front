import React, {Fragment, useEffect} from "react";
import {Route,Switch,withRouter} from 'react-router-dom';
import {useDispatch} from "react-redux";
import jwt from "jsonwebtoken";

import MainLayout from "../Layout/mainLayout";
import Login from "./Login";
import Register from "./Register";
import {clearUser, setUser} from "../redux/action/user";
import Logout from "./Logout";
import MyPage from "./MyPage";
import {myImages} from "../redux/action/myImages";
import {myGalleries} from "../redux/action/myGalleries";
import {allGallery} from "../redux/action/allGallery";
import AllGalleries from "../Component/Gallery/AllGalleries";
import NotFound from "./NotFound";
import Images from "../Component/Gallery/Images";

const ImageGallery = (props) => {
    const dispatch=useDispatch();

    useEffect(()=>{
        const init = async () => {
            const token=localStorage.getItem("token");
            if(token) {
                // const decodeToken=jwt.decode(token, {complete: true})
                // const expDate=decodeToken.payload.exp;
                // const date=Date.now()/1000;
                // if(expDate<date){
                //     localStorage.removeItem("token");
                //     dispatch(clearUser());}
                await dispatch(setUser());
                await dispatch(myImages());
                await dispatch(myGalleries());
                await dispatch(allGallery());
        }}
        init();
    },[])

    return(
        <Fragment>
             <MainLayout>
                 <Switch>
                     <Route path="/MyPage" component={MyPage} />
                     <Route path="/Login" component={Login} />
                     <Route path="/Logout" component={Logout} />
                     <Route path="/Register" component={Register} />
                     <Route path="/galleries/:id" component={Images} />
                     <Route path="/" exact component={AllGalleries} />
                     <Route path="*" exact component={NotFound} />
                 </Switch>
             </MainLayout>
        </Fragment>

    )

}
export default withRouter(ImageGallery);