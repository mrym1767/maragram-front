import React, {Fragment,useContext} from "react";
import {Helmet} from "react-helmet";
import Context from "../Context/Context";
import {NavLink} from "react-router-dom";

const Login = () => {
    const registerContext=useContext(Context);
    const {
        email,
        setEmail,
        password,
        setPassword,
        valid,
        handleSubmitLogin
    }=registerContext;
    return(
        <Fragment>
        <div className="login">
            <Helmet> <title>ماراگرام | ورود</title> </Helmet>
            <NavLink to="/">
                <img className=" mx-auto d-block" src="../img/logo1.png" style={{height:"28.6px"}} />
                <h2>Maragram</h2>
            </NavLink>

            <form onSubmit={handleSubmitLogin}>
                <div className="form-group">
                    <input type="text" id="username" className="form-control" placeholder="ایمیل" value={email}
                           onChange={e=> setEmail(e.target.value)} />
                        {valid.current.message("email",email,"required|email")}
                </div>

                <div className="form-group">
                    <input type="password" id="email" className="form-control" placeholder="رمز عبور" value={password}
                           onChange={e=> setPassword(e.target.value)}/>
                           {valid.current.message("password", password, "required")}
                </div>

                <button className="btn btn-block">ورود</button>
            </form>
        </div>

            <div className="login-2 mx-auto">
                <h6>حساب کاربری ندارید؟ <NavLink to="/Register"> ثبت نام کنید</NavLink></h6>
            </div>
        </Fragment>
    )
}

export default Login;