import React from "react";
import {useDispatch} from "react-redux";
import {clearUser} from "../redux/action/user";

const Logout = (props) => {
    const dispatch=useDispatch();
    localStorage.removeItem("token")
    dispatch(clearUser());
    props.history.replace("/Login");

    return null;
}

export default Logout;