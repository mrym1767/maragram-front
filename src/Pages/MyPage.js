import React from "react";
import {Route, Switch} from "react-router-dom";
import MyGalleries from "../Component/Gallery/MyGalleries";
import MyImages from "../Component/Gallery/MyImages";
import SecondNav from "../Component/SecondNav";
import {useSelector} from "react-redux";
import MyGallery from "../Component/Gallery/MyGallery";
import UploadImage from "../Component/Dialog/UploadImage";
import CreateGallery from "../Component/Dialog/CreateGallery";

const MyPage = () => {
    const user=useSelector(state=>state.user);
return(
    <div className="">

        <div className="row justify-content-start" style={{color:"#383d41"}}>
            <div className="profile-photo">
                <i className="fa fa-5x fa-user-circle"></i>
            </div>

            <div className="profile-name">
                <p>
                    <strong>{`${user.firstName} ${user.lastName}`}</strong> <br/>
                    <small> {`آیدی: ${user.id}`} </small>
                </p>
            </div>
        </div>

        <SecondNav/>

        <Switch>
            <Route path="/MyPage/CreateGallery" component={CreateGallery} />
            <Route path="/MyPage/MyGalleries/:id" component={MyGallery} />
            <Route path="/MyPage/MyGalleries" exact component={MyGalleries} />
            <Route path="/MyPage/MyImages" exact component={MyImages} />
            <Route path="/MyPage/UploadPhoto" exact component={UploadImage} />
        </Switch>
    </div>
);
}

export default MyPage;