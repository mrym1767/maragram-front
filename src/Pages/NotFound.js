import React from "react";
import {NavLink} from "react-router-dom";

const NotFound = () => {
    return(
        <div className="margin-top-sm">
            <div className="alert alert-light">
                <p className="display-3">404</p>
                <p>متاسفیم صفحه مورد نظر یافت نشد.</p>
                <NavLink className="alert-link" to="/">بازگشت به صفحه اصلی</NavLink>
            </div>
        </div>
    )
}

export default NotFound;