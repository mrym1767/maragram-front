import React, {useContext,Fragment} from "react";
import {Helmet} from "react-helmet";
import Context from "../Context/Context";
import {NavLink} from "react-router-dom";

const Register = () => {
    const loginContext=useContext(Context);
    const {
        firstName,
        setFirstName,
        lastName,
        setLastName,
        email,
        setEmail,
        password,
        setPassword,
        valid,
        rules,
        setRules,
        handleSubmitRegister
    }=loginContext;

    return(
        <Fragment>
        <div className="login">
            <Helmet> <title>گالری عکس | ثبت نام</title> </Helmet>
            <NavLink to="/">
                <img className=" mx-auto d-block" src="../img/logo1.png" style={{height:"28.6px"}} />
            </NavLink>

            <form className="" onSubmit={handleSubmitRegister}>
                <div className="form-group">
                    <input
                        type="text"
                        id="firstName"
                        name="firstName"
                        className="form-control"
                        placeholder="نام"
                        value={firstName}
                        onChange={e => {
                            setFirstName(e.target.value);
                            valid.current.showMessageFor("firstName")}} />
                            {valid.current.message("firstName", firstName, "required|alpha|max:15")}
                </div>

                <div className="form-group">
                    <input
                        type="text"
                        name="lastName"
                        id="lastName"
                        className="form-control"
                        placeholder="نام خانوادگی"
                        value={lastName}
                        onChange={e => {
                            setLastName(e.target.value);
                            valid.current.showMessageFor("lastName")}} />
                            {valid.current.message("lastName", lastName, "required|alpha")}
                </div>

                <div className="form-group">
                        <input type="email"
                               id="email"
                               className="form-control"
                               placeholder="ایمیل"
                               value={email}
                               onChange={e => {
                                   setEmail(e.target.value)
                                   valid.current.showMessageFor("email") }} />
                        {valid.current.message("email", email, "required|email")}
                </div>

                <div className="form-group">
                        <input
                            type="password"
                            id="password"
                            className="form-control"
                            value={password}
                            placeholder="رمزعبور"
                            onChange={e => {
                                setPassword(e.target.value)
                                valid.current.showMessageFor("password")
                            }}/>
                        {valid.current.message("password", password, "required|min:5|max:15")}
                </div>


                {/*<div className="form-group">*/}
                {/*        <label className="col-form-label">جنسیت:</label>*/}
                {/*    <div className="form-check form-check-inline">*/}
                {/*        <input className="form-check-input" type="radio" name="jensiat" id="inlineRadio1"*/}
                {/*           value="woman"/>*/}
                {/*           <label className="form-check-label mr-3" htmlFor="inlineRadio1">زن</label>*/}
                {/*    </div>*/}
                {/*    <div className="form-check form-check-inline">*/}
                {/*        <input className="form-check-input" type="radio" name="jensiat" id="inlineRadio2"*/}
                {/*           value="man"/>*/}
                {/*           <label className="form-check-label" htmlFor="inlineRadio2">مرد</label>*/}
                {/*    </div>*/}
                {/*</div>*/}

                <button className="btn btn-block">ثبت</button>
            </form>
        </div>

            <div className="login-2 mx-auto">
                <h6>قبلا ثبت نام کردید؟ <NavLink to="/Login"> وارد شوید</NavLink></h6>
            </div>
        </Fragment>
    )
}

export default Register;