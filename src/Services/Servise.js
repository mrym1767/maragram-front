import axios from "axios";
import http from "./httpServices";
import config from "./config.json"

export const registerUser = user => {
    return http.post(`${config.galleryapi}/users`,JSON.stringify(user))
}

export const loginUser = user => {
    return http.post(`${config.galleryapi}/users/login`,JSON.stringify(user))
}

export const getUser = () => {
    return http.get(`${config.galleryapi}/users/profile`)
}

export const getAllGallery = () => {
    return http.get(`${config.galleryapi}/galleries`)
}

export const uploadPhoto = photo => {
    return http.post(`${config.galleryapi}/photos`, photo);
};

export const getMyImages = () => {
    return http.get(`${config.galleryapi}/photos`)
};

export const getMyGalleries = () => {
    return http.get(`${config.galleryapi}/users/galleries`)
};

export const createGallery = title => {
    return http.post(`${config.galleryapi}/galleries`,JSON.stringify({title}))
};

export const getGalleryPosts = id => {
    return http.get(`${config.galleryapi}/galleries/${id}/posts`)
}

export const addPhoto = (galleryId,photoId) => {
    return http.post(`${config.galleryapi}/galleries/${galleryId}/posts`, {"photo":photoId})
}

export const likePhoto = (galleryId,postId) => {
    return http.put(`${config.galleryapi}/galleries/${galleryId}/posts/${postId}/like`)
}

export const disLikePhoto = (galleryId,postId) => {
    return http.delete(`${config.galleryapi}/galleries/${galleryId}/posts/${postId}/like`)
}