import axios from 'axios';
import {toast} from "react-toastify";
import {errorMessage} from "../Utils/Messages";

const token=localStorage.getItem("token");
if(token) axios.defaults.headers.common["Authorization"]=`Bearer ${token}`;

axios.defaults.headers.post={"Content-Type":"application/json","Access-Control-Allow-Origin":"*"};

axios.interceptors.response.use(null,error => {
    const expected= error.response && error.response.status>= 400 && error.response.status<=500;
    if (!expected){
        console.log(error);
        toast.error("مشکلی از سمت سرور پیش آمده",{
            position:"bottom-right",
            closeOnClick:true,
            autoClose:1600
        })
    } else if (expected) errorMessage("ابتدا باید وارد شوید.");
    return Promise.reject(error);
})

export default {
    get:axios.get,
    post:axios.post,
    put:axios.put,
    delete:axios.delete
};