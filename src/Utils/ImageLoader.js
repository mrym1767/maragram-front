import React from "react";
import Img from "react-image";
import SyncLoader from "react-spinners/SyncLoader";
import {withRouter} from "react-router";

const ShowImage = ({ image ,location,match}) => {
    return (
        <Img
            className={location.pathname==="/MyPage/MyImages" || location.pathname===`/MyPage/MyGalleries/${match.params.id}`?"my-img":"all-img"}
            src={[
                `${image}`,
                "https://via.placeholder.com/150x100"
            ]}
            loader={
                <div className="text-center mx-auto">
                    <SyncLoader loading={true} color={"white"} />
                </div>
            }
        />
    );
};

export default withRouter(ShowImage);
