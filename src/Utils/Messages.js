import {toast} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export const successMessage = message => {
    toast.success(`${message}`,
        {
            position: "bottom-right",
            closeOnClick: true,
            autoClose: 1600
        });
};

export const errorMessage = message => {
    toast.error(`${message}`,
        {
            position: "bottom-right",
            closeOnClick: true,
            autoClose: 1600
        });
}