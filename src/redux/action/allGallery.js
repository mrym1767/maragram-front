import {getAllGallery} from "../../Services/Servise";

export const allGallery = () => {
    return async dispatch => {
        const {data} =await getAllGallery();
        await dispatch({type:"GET_ALL_GALLERIES",payload:data.galleries})
    }
}