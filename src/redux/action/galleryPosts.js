import {getGalleryPosts} from "../../Services/Servise";

export const galleryPosts = id => {
    return async dispatch => {
        const {data} = await getGalleryPosts(id);
        await dispatch({type:"GET_GALLERY_POSTS", payload:data.posts});
    }
}