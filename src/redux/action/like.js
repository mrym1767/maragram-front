import {disLikePhoto, likePhoto} from "../../Services/Servise";

export const likePhotos = (galleryId,postId) => {
    return async dispatch => {
        await likePhoto(galleryId,postId);
        await dispatch({type:"LIKE"})
    }
}

export const disLikePhotos = (galleryId,postId) => {
    return async dispatch => {
        await disLikePhoto(galleryId,postId);
        await dispatch({type:"DIS_LIKE"})
    }
}