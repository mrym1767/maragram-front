import {createGallery, getMyGalleries} from "../../Services/Servise";
import {successMessage} from "../../Utils/Messages";

export const myGalleries = () => {
    return async dispatch => {
        const {data}= await getMyGalleries();
        await dispatch({type:"GET_MY_GALLERIES",payload:data.galleries});
    }
}

export const newGallery = title => {
    return async (dispatch,getState)=> {
        const {data,status} = await createGallery(title);
        if (status===200) successMessage("گالری جدید اضافه شد.");
        await dispatch({type:"NEW_GALLERY",payload: [...getState().myGalleries, data]})
    }
}