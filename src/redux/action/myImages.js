import {getMyImages, uploadPhoto} from "../../Services/Servise";
import {successMessage} from "../../Utils/Messages";

export const myImages = () => {
    return async dispatch => {
        const {data} = await getMyImages();
        await dispatch({type:"GET_MY_IMAGES",payload:data.photos})
    }
};

export const newImage = image => {
    return async (dispatch,getState)=> {
        const {data,status} = await uploadPhoto(image);
        if (status===200) successMessage("عکس جدید اضافه شد.");
        await dispatch({type:"NEW_IMAGE",payload: [...getState().myImages, data]})
    }
}
