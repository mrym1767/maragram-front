import {getUser} from "../../Services/Servise";

export const setUser = () => {
    return async dispatch => {
        const {data}=await getUser();
        await dispatch({type:"SET_USER",payload:data.user})
    }
}

export const clearUser= () => {
    return async dispatch => {
        await dispatch({type: "CLEAR_USER",payload:{}})
    }
}