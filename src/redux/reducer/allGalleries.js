export const allGalleriesReducer = (state=[],action) => {
    switch (action.type) {
        case "GET_ALL_GALLERIES":
            return [...action.payload];
        default:
            return state;
    }
}