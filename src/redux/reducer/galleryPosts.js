export const galleryPostsReducer = (state=[],action) => {
    switch (action.type) {
        case "GET_GALLERY_POSTS":
            return [...action.payload];
        default:
            return state;
    }
}