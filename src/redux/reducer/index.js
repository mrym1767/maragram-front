import {combineReducers} from "redux";
import {userReducer} from "./user";
import {myImagesReducer} from "./myImages";
import {myGalleriesReducer} from "./myGalleries";
import {galleryPostsReducer} from "./galleryPosts";
import {allGalleriesReducer} from "./allGalleries";
import {likeReducer} from "./like";

export const reducers = combineReducers({
    user:userReducer,
    allGalleries:allGalleriesReducer,
    myImages:myImagesReducer,
    myGalleries:myGalleriesReducer,
    galleryPosts:galleryPostsReducer,
    likePost:likeReducer
})