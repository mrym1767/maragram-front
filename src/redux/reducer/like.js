export const likeReducer = (state="",action) => {
    switch (action.type) {
        case "LIKE":
            return "true";
        case "DIS_LIKE":
            return "false";
        default:
            return state;
    }
}