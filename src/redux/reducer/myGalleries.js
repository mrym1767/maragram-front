export const myGalleriesReducer = (state=[],action) => {
    switch (action.type) {
        case "GET_MY_GALLERIES":
            return [...action.payload];
        case "NEW_GALLERY":
            return [...action.payload];
        default:
            return state;
    }
}