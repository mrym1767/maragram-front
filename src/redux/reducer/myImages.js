export const myImagesReducer = (state=[], action) => {
    switch (action.type) {
        case "GET_MY_IMAGES":
            return [...action.payload];
        case "NEW_IMAGE":
            return [...action.payload];
        default:
            return state;
    }
}