import {createStore,applyMiddleware,compose} from "redux";
import thunk from "redux-thunk";
import {reducers} from "../reducer";
import {loadingBarMiddleware} from "react-redux-loading-bar";
import {setUser} from "../action/user";

export const store=createStore(reducers, compose(
    applyMiddleware(thunk,loadingBarMiddleware())
))

store.dispatch(setUser());

store.subscribe(()=>{console.log(store.getState())});